# Maintainer: Alexander Epaneshnikov <alex19ep@archlinux.org>

_langs=(Albanian Brazilian-Portuguese English Esperanto Georgian Kyrgyz Macedonian
        Polish Russian Tatar Ukrainian)
_voices=(alan aleksandr aleksandr-hq anatol anna arina artemiy azamat
         bdl clb elena evgeniy-eng evgeniy-rus hana irina kiko Leticia-F123
         lyubov magda marianna mikhail natalia natan natia nazgul pavel slt spomenka
         suze talgat tatiana victoria vitaliy volodymyr yuriy)
declare -A _voice_lang=(
	[alan]=english
	[aleksandr]=russian
	[aleksandr-hq]=russian
	[anatol]=ukrainian
	[anna]=russian
	[arina]=russian
	[artemiy]=russian
	[azamat]=kyrgyz
	[bdl]=english
	[clb]=english
	[elena]=russian
	[evgeniy-eng]=english
	[evgeniy-rus]=russian
	[hana]=albanian
	[irina]=russian
	[kiko]=macedonian
	[Leticia-F123]=brazilian-portuguese
	[lyubov]=english
	[magda]=polish
	[marianna]=ukrainian
	[mikhail]=russian
	[natalia]=ukrainian
	[natan]=polish
	[natia]=georgian
	[nazgul]=kyrgyz
	[pavel]=english
	[slt]=english
	[spomenka]=esperanto
	[suze]=macedonian
	[talgat]=tatar
	[tatiana]=russian
	[victoria]=russian
	[vitaliy]=russian
	[volodymyr]=ukrainian
	[yuriy]=russian
)
pkgbase=rhvoice
pkgname=(rhvoice)
for l in "${_langs[@]}"; do
	pkgname+=(rhvoice-language-${l,,})
done
for v in "${_voices[@]}"; do
	pkgname+=(rhvoice-voice-${v,,})
done
pkgver=1.8.0
pkgrel=1
pkgdesc="Free and open source speech synthesizer for Russian and other languages"
arch=('x86_64')
url="https://github.com/RHVoice/RHVoice"
license=('GPL3')
depends=('speech-dispatcher' 'libpulse' 'portaudio')
makedepends=('scons' 'libao')
source=(https://github.com/RHVoice/RHVoice/releases/download/$pkgver/$pkgbase-$pkgver.tar.gz{,.sig})
validpgpkeys=(6C7F7F22E0152A6FD5728592DAD6F3056C897266)
sha512sums=('e1c8c17f048fc466ccc6de285d379e2917edeb30b787c0c95003a00ff7f9a52669279de5f6e2cd472a92e613e30e79cb32ab6eefe8d4df497b00bd36c0ac981a'
            'SKIP')
b2sums=('c90337c3ff16346cec1520427976121e6a79861d40006502768d299aca6705bf4c1f524b5b5e78bfcf29dcabb403d7130a3ade22051db45e32adceb16afd8e08'
        'SKIP')

build() {
	cd "${pkgbase}-${pkgver}"
	jobs=$(expr "$MAKEFLAGS" : '.*\(-j[0-9]*\).*') || true
	scons prefix="/usr" sysconfdir="/etc" CPPFLAGS="$CPPFLAGS" CFLAGS="$CFLAGS" \
	      CXXFLAGS="$CXXFLAGS"  LINKFLAGS="$LDFLAGS" $jobs
}

package_rhvoice() {
	backup=('etc/RHVoice/RHVoice.conf')
	groups=('rhvoice')
	install=rhvoice.install
	optdepends=('libao: for ao backend')
	optdepends+=("${_rhvoice_optdepends[@]}")
	cd "${pkgbase}-${pkgver}"
	jobs=$(expr "$MAKEFLAGS" : '.*\(-j[0-9]*\).*') || true
	scons install DESTDIR="${pkgdir}" prefix="/usr" sysconfdir="/etc" \
	      CPPFLAGS="$CPPFLAGS" CFLAGS="$CFLAGS" CXXFLAGS="$CXXFLAGS" \
	      LINKFLAGS="$LDFLAGS" $jobs
	# remove split data
	rm -r "${pkgdir}/usr/share/"
}

# package functions for languages
for l in "${_langs[@]}"; do
	eval "
package_rhvoice-language-${l,,}() {
	pkgdesc=\"Rhvoice ${l,,} language\"
	depends=('rhvoice')
	groups=('rhvoice')
	optdepends=()

	cd \"${pkgbase}-${pkgver}\"
	if [[ -d data/languages/$l/userdict ]]; then
		install -vDm644 data/languages/$l/userdict/src/*.txt -t \"\${pkgdir}/usr/share/RHVoice/languages/$l/userdict/src\"
	fi
	if [[ $l == Macedonian ]]; then
		license=(AGPL3)
	fi
	install -vDm644 data/languages/$l/*.* -t \"\${pkgdir}/usr/share/RHVoice/languages/$l\"
}
    "
done

# package functions for voices
for v in "${_voices[@]}"; do
	_rhvoice_optdepends+=(rhvoice-voice-${v,,})
	_voicelang=${_voice_lang[${v}]}
	if [[ -z ${_voicelang} ]]; then
		echo "Missing voice language mapping for ${v}"
		exit 1
	fi
	eval "
package_rhvoice-voice-${v,,}() {
	pkgdesc=\"Rhvoice ${v,,} voice for ${_voicelang,,} language\"
	depends=(rhvoice-language-${_voicelang,,})
	groups=('rhvoice')
	optdepends=()
	if [[ $v == aleksandr-hq || $v == arina || $v == artemiy || $v == evgeniy-eng \
	      || $v == evgeniy-rus || $v == lyubov || $v == magda || $v == marianna \
	      || $v == mikhail || $v == natan || $v == pavel || $v == victoria \
	      || $v == vitaliy || $v == volodymyr || $v == yuriy ]]; then
		license=('custom:CC-BY-NC-ND-4.0')
	elif [[ $v == kiko || $v == hana ]]; then
		license=('custom:CC-BY-NC-SA-4.0')
	elif [[ $v == Leticia-F123 ]]; then
		license=('custom:CC-BY-SA-4.0')
	elif [[ $v == natia || $v == talgat || $v == suze ]]; then
		license=('custom')
	fi

	_voicelang_data=\$(grep \"language\" \$pkgbase-\$pkgver/data/voices/$v/voice.info | sed 's/^.*=//')
	if [[ ${_voicelang} != \"\${_voicelang_data,,}\" ]]; then
		error \"Declared language for voice does not match rhvoice data, expected: ${_voicelang}, actual: \${_voicelang_data,,}\"
		exit 1
	fi

	cd \"${pkgbase}-${pkgver}\"
	install -vDm644 data/voices/$v/16000/* -t \"\${pkgdir}/usr/share/RHVoice/voices/$v/16000\"
	install -vDm644 data/voices/$v/24000/* -t \"\${pkgdir}/usr/share/RHVoice/voices/$v/24000\"
	install -vDm644 data/voices/$v/{voice.info,voice.params} -t \"\${pkgdir}/usr/share/RHVoice/voices/$v\"
	case \$license in
	'custom:CC-BY-NC-ND-4.0')
		install -vDm644 licenses/by-nc-nd-4.0.txt \"\${pkgdir}/usr/share/licenses/\${pkgname}/license\"
	;;
	'custom:CC-BY-NC-SA-4.0')
		install -vDm644 licenses/by-nc-sa-4.0.txt \"\${pkgdir}/usr/share/licenses/\${pkgname}/license\"
	;;
	'custom:CC-BY-SA-4.0')
		install -vDm644 licenses/by-sa-4.0.txt \"\${pkgdir}/usr/share/licenses/\${pkgname}/license\"
	;;
	'custom')
	if [[ $v == natia ]]; then
		install -vDm644 licenses/voices/natia/license-eng.txt \"\${pkgdir}/usr/share/licenses/\${pkgname}/license\"
	elif [[ $v == talgat ]]; then
		install -vDm644 licenses/voices/talgat/license-eng.txt \"\${pkgdir}/usr/share/licenses/\${pkgname}/license\"
	fi
	;;
esac
}
    "
done
